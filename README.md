# To run tests
* clone the project 
```git clone```
* open the project in PyCharm
* [configure PyCharm to use pytest as default test runner](https://stackoverflow.com/a/6397315/1562282)
* click on green triangle near test that you want to run

# HTML Report
![Alt text](doc/html.png?raw=true "HTML Report")
# Allure Report
![Alt text](doc/allure.png?raw=true "Allure Report")
# Re-run failed tests
![Alt text](doc/re-run.png?raw=true "Re-run Failed")

* if you want to run only api tests you need to use: pytest -v -m api
* if you want to run only ui tests you need to use: pytest -v -m ui
* if you want to run all tests you need to use: pytest

```WARNING: If you want UI script to go through successfully uncomment db.remove('test user3') in tests/ui/src/functions/user_functions.py```