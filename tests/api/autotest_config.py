import os

HOSTNAME = os.environ.get('HOSTNAME', 'https://jsonplaceholder.typicode.com')
