import requests
import tests.api.autotest_config as config
import tests.api.data.paths as paths
import os
import json as simplejson
import jsonschema
import urllib3
urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)

HOSTNAME = config.HOSTNAME


def get_users():
    response = requests.get(HOSTNAME + paths.USERS, verify=False)
    assert response.status_code == 200
    return response.json()


def json_validation(user_data):
    script_dir = os.path.dirname(__file__)
    rel_path = '../data/jsons/users.json'
    file_path = os.path.join(script_dir, rel_path)

    with open(str(file_path), 'r') as f:
        schema_data = f.read()
    schema = simplejson.loads(schema_data)

    if user_data[0]:
        jsonschema.validate(instance=user_data[0], schema=schema)


def check_quantity_objects(user_data, expected_objects):
    assert len(user_data) == expected_objects


def check_object_is_not_exist(object_id):
    response = requests.get(HOSTNAME + paths.USERS + "/" + object_id, verify=False)
    assert response.status_code == 404
