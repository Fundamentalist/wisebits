import allure
import tests.api.functions.users as users
import pytest
import urllib3
urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)

user_data: str


@pytest.mark.api
class TestUsers:

    @allure.severity('blocker')
    @allure.title('successful get users')
    def test_get_users(self):
        global user_data
        user_data = users.get_users()

    @allure.title('JSON validation')
    def test_json_validation_for_users(self):
        users.json_validation(user_data)

    @allure.title('check response has 10 JSON objects')
    def test_quantity_user_objects(self):
        users.check_quantity_objects(user_data, 10)

    @allure.title('negative cases. object is not exist')
    def test_check_object_is_not_exist(self):
        users.check_object_is_not_exist("11")
        users.check_object_is_not_exist("0")
        users.check_object_is_not_exist("-1")
        users.check_object_is_not_exist("string")
        users.check_object_is_not_exist("1.1")


