import psycopg2
from contextlib import closing


class MantisUserTable:

    def get_users(self):
        users = []
        with closing(psycopg2.connect(dbname='mantisbt',
                                      user='mantisbt',
                                      password='mantisbt2019',
                                      host='146.185.143.168',
                                      port='5432',
                                      sslmode='require')) as conn:
            with conn.cursor() as cursor:
                cursor.execute('select username from mantis_user_table;')
                users = [desc[0] for desc in cursor]

        return users
