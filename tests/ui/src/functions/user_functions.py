
def check_equals_users_from_db_and_web(users_from_db, users_from_web):
    db = sorted(users_from_db)
    web = sorted(users_from_web)

    # TODO If you want the script to go through successfully uncomment this line
    # db.remove('test user3')

    result = list(set(db) ^ set(web))

    if result != []:
        raise IOError("difference is " + str(result))
