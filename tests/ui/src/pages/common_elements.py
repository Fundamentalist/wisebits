from selenium.webdriver.common.by import By


class CommonElements:
    MANAGE_BUTTON = (By.CSS_SELECTOR, '[href="/manage_overview_page.php"]')
    driver = None

    def __init__(self, driver):
        self.driver = driver

    def click_on_manage_button(self):
        self.driver.find_element(*self.MANAGE_BUTTON).click()
