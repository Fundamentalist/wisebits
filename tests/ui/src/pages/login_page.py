from selenium.webdriver.common.by import By
from tests.ui.autotest_config import *


class LoginPage:
    SUBMIT_BUTTON = (By.CSS_SELECTOR, '[type="submit"]')
    LOGIN_FIELD = (By.CSS_SELECTOR, "#username")
    driver = None

    def __init__(self, driver):
        self.driver = driver

    def open(self):
        self.driver.get(HOSTNAME)
        return self

    def set_login(self):
        self.driver.find_element(*self.LOGIN_FIELD).clear()
        self.driver.find_element(*self.LOGIN_FIELD).send_keys(LOGIN)

    def click_on_submit_button(self):
        self.driver.find_element(*self.SUBMIT_BUTTON).click()

