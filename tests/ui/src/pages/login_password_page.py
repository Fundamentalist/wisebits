from selenium.webdriver.common.by import By
from tests.ui.autotest_config import *


class LoginPasswordPage:
    SUBMIT_BUTTON = (By.CSS_SELECTOR, '[type="submit"]')
    PASSWORD_FIELD = (By.CSS_SELECTOR, "#password")
    driver = None

    def __init__(self, driver):
        self.driver = driver

    def open(self):
        self.driver.get(HOSTNAME)
        return self

    def set_password(self):
        self.driver.find_element(*self.PASSWORD_FIELD).clear()
        self.driver.find_element(*self.PASSWORD_FIELD).send_keys(PASSWORD)

    def click_on_submit_button(self):
        self.driver.find_element(*self.SUBMIT_BUTTON).click()

