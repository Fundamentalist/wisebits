from selenium.webdriver.common.by import By
from tests.ui.autotest_config import *
from tests.ui.src.pages.common_elements import CommonElements
from tests.ui.paths import MANAGE_OVERVIEW_PAGE


class ManageOverviewPage(CommonElements):
    MANAGE_USER_BUTTON = (By.CSS_SELECTOR, '[href="/manage_user_page.php"]')
    driver = None

    def __init__(self, driver):
        self.driver = driver

    def check_url(self):
        assert self.driver.current_url == HOSTNAME + MANAGE_OVERVIEW_PAGE

    def click_on_manage_user_column(self):
        self.driver.find_element(*self.MANAGE_USER_BUTTON).click()

