from tests.ui.autotest_config import HOSTNAME
from selenium.webdriver.common.by import By
from tests.ui.src.pages.common_elements import CommonElements
from tests.ui.paths import MANAGE_USER_PAGE


class ManageUserPage(CommonElements):
    USERS = (By.CSS_SELECTOR, 'tbody a')
    driver = None

    def __init__(self, driver):
        self.driver = driver

    def check_url(self):
        assert self.driver.current_url == HOSTNAME + MANAGE_USER_PAGE

    def get_users(self):
        users = self.driver.find_elements(*self.USERS)
        user_list = []
        for user in users:
            user_list.append(user.text)
        return user_list


