from selenium.webdriver.common.by import By
from tests.ui.autotest_config import *
from tests.ui.src.pages.common_elements import CommonElements
from tests.ui.paths import MY_VIEW_PAGE


class MyViewPage(CommonElements):
    SUBMIT_BUTTON = (By.CSS_SELECTOR, '[type="submit"]')
    driver = None

    def __init__(self, driver):
        self.driver = driver

    def check_url(self):
        assert self.driver.current_url == HOSTNAME + MY_VIEW_PAGE


