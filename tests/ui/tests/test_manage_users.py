import pytest
from selenium import webdriver
from webdriver_manager.chrome import ChromeDriverManager
from tests.ui.src.db.mantisbt.mantis_user_table import MantisUserTable
from tests.ui.src.pages.login_page import LoginPage
from tests.ui.src.pages.login_password_page import LoginPasswordPage
from tests.ui.src.pages.my_view_page import MyViewPage
from tests.ui.src.pages.common_elements import CommonElements
from tests.ui.src.pages.manage_overview_page import ManageOverviewPage
from tests.ui.src.pages.manage_user_page import ManageUserPage
import tests.ui.src.functions.user_functions as user_functions


@pytest.mark.ui
class TestManageUsers:

    def setup_method(self):
        self.driver = webdriver.Chrome(executable_path=ChromeDriverManager().install())
        self.driver.maximize_window()

        self.mantis_user_table = MantisUserTable()

        self.login_page = LoginPage(self.driver)
        self.login_password_page = LoginPasswordPage(self.driver)
        self.my_view_page = MyViewPage(self.driver)
        self.common_elements = CommonElements(self.driver)
        self.manage_overview_page = ManageOverviewPage(self.driver)
        self.manage_user_page = ManageUserPage(self.driver)

    def test_equals_users_from_db_and_web(self):
        self.login_page.open()
        self.login_page.set_login()
        self.login_page.click_on_submit_button()
        self.login_password_page.set_password()
        self.login_page.click_on_submit_button()
        self.my_view_page.check_url()

        self.common_elements.click_on_manage_button()
        self.manage_overview_page.check_url()
        self.manage_overview_page.click_on_manage_user_column()
        self.manage_user_page.check_url()
        users_from_web = self.manage_user_page.get_users()

        users_from_db = self.mantis_user_table.get_users()

        user_functions.check_equals_users_from_db_and_web(users_from_db, users_from_web)

    def teardown_method(self):
        self.driver.close()
